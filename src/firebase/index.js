import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/storage'
import 'firebase/database'
import 'firebase/firestore'

export const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyAI5mTrjbPjK4jOvMT3VPhUUl_4iOylMJM",
    authDomain: "guia-bonito.firebaseapp.com",
    databaseURL: "https://guia-bonito.firebaseio.com",
    projectId: "guia-bonito",
    storageBucket: "guia-bonito.appspot.com",
    messagingSenderId: "698607284213",
    appId: "1:698607284213:web:902a01b34aacd41a6c3abc",
    measurementId: "G-323DVHMGZV"
})

export default function install (Vue) {
    Object.defineProperty(Vue.prototype, '$firebase', {
        get () {
            return firebaseApp
        }
    })
}