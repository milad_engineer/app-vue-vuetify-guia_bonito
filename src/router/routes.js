export default [
    //@@@@@----- PRINCIPAIS
    {
    path: '/',
    name: 'Home',
    // route level code-splitting
    // isso gera um arquivo separado para esta rota
    // que e carregado no momento que eh chamado
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
    },

    //@@@@@----- DE LOGIN

    //@@@@@----- DO USUARIO LOGADO

    {
    path: '/userpage',
    name: 'User Page',
    component: () => import(/* webpackChunkName: "userpage" */ '../views/user/user_page.vue')
    },

    //@@@@@----- BONITO

    {
    path: '/agencias',
    name: 'Agencias',
    component: () => import(/* webpackChunkName: "agencias" */ '../views/bonito/agencias.vue')
    },

    {
    path: '/chegar',
    name: 'Chegar',
    component: () => import(/* webpackChunkName: "chegar" */ '../views/bonito/chegar.vue')
    },

    {
    path: '/gastronomia',
    name: 'Gastronomia',
    component: () => import(/* webpackChunkName: "gastronomia" */ '../views/bonito/gastronomia.vue')
    },

    {
    path: '/noticias',
    name: 'Noticias',
    component: () => import(/* webpackChunkName: "noticias" */ '../views/bonito/noticias.vue')
    },

    {
    path: '/restaurantes',
    name: 'Restaurantes',
    component: () => import(/* webpackChunkName: "restaurantes" */ '../views/bonito/restaurantes.vue')
    },

    //@@@@@----- FOTOS

    {
    path: '/galeria',
    name: 'Galeria',
    component: () => import(/* webpackChunkName: "galeria" */ '../views/fotos/galeria.vue')
    },

    //@@@@@----- HOTEIS

    {
    path: '/hoteis',
    name: 'Hotéis',
    component: () => import(/* webpackChunkName: "hoteis" */ '../views/hoteis/hoteis.vue')
    },

    {
    path: '/hoteis_links',
    name: 'Hoteis',
    component: () => import(/* webpackChunkName: "hoteis_links" */ '../views/hoteis/hoteis_links.vue')
    },

    //@@@@@----- TURISMO

    {
    path: '/aventuras',
    name: 'Aventuras',
    component: () => import(/* webpackChunkName: "aventuras" */ '../views/turismo/aventuras.vue')
    },

    {
    path: '/balnearios',
    name: 'Balnearios',
    component: () => import(/* webpackChunkName: "balnearios" */ '../views/turismo/balnearios.vue')
    },

    {
    path: '/cachoeiras',
    name: 'Cachoeiras',
    component: () => import(/* webpackChunkName: "cachoeiras" */ '../views/turismo/cachoeiras.vue')
    },

    {
    path: '/flutuacao',
    name: 'Flutuacao',
    component: () => import(/* webpackChunkName: "cachoeiras" */ '../views/turismo/flutuacao.vue')
    },

    {
    path: '/grutas',
    name: 'Grutas',
    component: () => import(/* webpackChunkName: "cachoeiras" */ '../views/turismo/grutas.vue')
    },

    {
    path: '/mergulho',
    name: 'Mergulho',
    component: () => import(/* webpackChunkName: "mergulho" */ '../views/turismo/mergulho.vue')
    },

    {
    path: '/urbanos',
    name: 'Urbanos',
    component: () => import(/* webpackChunkName: "urbanos" */ '../views/turismo/urbanos.vue')
    },

    

    

    

    

    

    
]
