//EXEMPLOS DE CODIGO PARA MANIPULAR BANCO DE DADOS FIREBASE

const db = this.$firebase.firestone();
db.collection('images').get().then((snapshot) => {
    console.log(snapshot.docs); //pega todos os documentos dessa colection, mas nao os dados
    snapshot.docs.forEach(doc => {
        console.log(doc.data); //para cada um dos documentos vai retornar um objeto com os dados
    });
})

db.collection('home').get().then((querySnapshot) => {
    console.log("ESTES SÃO OS DOCS - HOME");
    console.log(querySnapshot.docs); //pega todos os documentos dessa colection, mas nao os dados
    console.log("ESTES SÃO OS DADOS DOS DOCS - HOME");
    querySnapshot.forEach(doc => {
        console.log(doc.data()); //para cada um dos documentos vai retornar um objeto com os dados
    });

    console.log("ESTES SÃO OS DADOS DOS DOCS - HOME - IMAGENS");
    var docImagens = db.collection("home").doc("imagens");
    docImagens.get().then(doc => {
      if (doc.exists) {
        console.log("Document data:", doc.data().imagens);
    } else {
        // doc.data() will be undefined in this case
        console.log("No such document!");
    }}
    )
})
var homeRef = db.collection("home");
homeRef.doc("images").set({
    carousel: [
        'https://saiadazonadeconforto.com.br/wp-content/uploads/2019/10/Flutua%C3%A7%C3%A3o-Rio-Sucuri.jpg',
        'https://abrilviagemeturismo.files.wordpress.com/2016/10/15-es39.jpeg?quality=70&strip=info&w=928',
        'https://www.bonitoway.com.br/public/posts/bonito_ms_setembro_e.png?1567544063',
        'https://www.bonitoway.com.br/public/produtos/zapi_zen_restaurante_bonito_ms_bonito_way_10-0_a.jpg',
        'https://media-cdn.tripadvisor.com/media/photo-s/0f/0d/6c/ba/zagaia-eco-resort-hotel.jpg',
      ],
    }
  );