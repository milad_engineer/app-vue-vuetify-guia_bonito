import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

//@@@@@----- STATE DA APLICACAO
const state = {
  counter: 0,
  user_token: '',
  hotel_ref: {
    id: '',
    type: 'hotel',
  },
  restaurante_ref: {
    id: '',
    type: 'restaurante',
  },
  estab_ref: {
    id: '',
    type: 'estabelecimento',
  }
}

//@@@@@----- MUTATIONS MUDA O STATE
//Chamam 2 parametros
const mutations = {
  setCounter (state, payload){
    //console.log('chamou Mutation com o valor: ' + payload);
    state.counter = payload;
  },

  setHotel (state, payload){
    state.hotel_ref.id = payload;
  }, 

  setRestaurante (state, payload){
    state.restaurante_ref = payload;
  }, 

  setEstab (state, payload){
    state.estab_ref = payload;
  }, 
}

//@@@@@----- ACTIONS MUDAM MUTATIONS
//Chamam 2 parametros 
const actions = {
  //o bjeto pode conter 3 parametros {commit, state, dispatch}, dispatch para char outra action
  actionCounter({commit}, todo){
    //console.log('chamou Action com o novo valor: ' + todo);
    commit('setCounter', todo);
  },

  changeHotel({commit}, todo){
    localStorage.setItem('hotel_id', todo); //colocando no localhost
    commit('setHotel', todo);
    
  }
}

//@@@@@----- OBJETO STORE GERAL CHAMANDO TODOS OS METODOS
const store = new Vuex.Store({state,mutations,actions,})
export default store;
